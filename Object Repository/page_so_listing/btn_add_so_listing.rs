<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_add_so_listing</name>
   <tag></tag>
   <elementGuidId>0c424227-f5a3-4dba-81b8-9be68f474ed6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//li[@id='ctl00_phDS_ds_ToolBar_insert']/div/qp-hyper-icon/div/div/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.au-target.main-icon-img.main-AddNew</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>903d30b5-6149-4c81-88ac-d277aa8abfbf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>au-target main-icon-img main-AddNew</value>
      <webElementGuid>091827eb-41c0-46f0-bbaf-3085a915c0b1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>au-target-id</name>
      <type>Main</type>
      <value>69</value>
      <webElementGuid>b1627d05-0d99-48de-8913-22159287eac6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_phDS_ds_ToolBar_insert&quot;)/div[@class=&quot;au-target qp-tool-bar-button&quot;]/qp-hyper-icon[@class=&quot;au-target&quot;]/div[@class=&quot;qp-hyper-icon au-target&quot;]/div[@class=&quot;au-target sprite-icon main-icon&quot;]/div[@class=&quot;au-target main-icon-img main-AddNew&quot;]</value>
      <webElementGuid>211807a2-f16f-4453-b0b4-3e2c056be91f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/page_detail_so/iframe_so_details</value>
      <webElementGuid>93f27ca7-ef41-4a1d-80aa-dcab7d24eb22</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='ctl00_phDS_ds_ToolBar_insert']/div/qp-hyper-icon/div/div/div</value>
      <webElementGuid>21c5b0ce-b36c-4285-8508-e03827aa1645</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[3]/div/qp-hyper-icon/div/div/div</value>
      <webElementGuid>5a6e7e39-e9ac-4f28-b132-079221f32df2</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
