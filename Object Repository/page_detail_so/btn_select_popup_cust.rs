<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_select_popup_cust</name>
   <tag></tag>
   <elementGuidId>65ddd8db-9251-4ae8-ac6f-ee13f7e9d8ba</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;ctl00_phF_form_t0_edCustomerID_pnl_tlb_ul&quot;)/li[1]/div[@class=&quot;toolsBtn connotation-success&quot;]/div[@class=&quot;toolBtnNormal&quot;][count(. | //*[(text() = '
							Select
						' or . = '
							Select
						') and @ref_element = 'Object Repository/iframe_sales_order_detail']) = count(//*[(text() = '
							Select
						' or . = '
							Select
						') and @ref_element = 'Object Repository/iframe_sales_order_detail'])]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.toolsBtn.connotation-success > div.toolBtnNormal</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Select - Customer'])[1]/following::div[4]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>5bb439ba-2b41-48f2-95fd-91ea35c27947</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>toolBtnNormal</value>
      <webElementGuid>3eb73fd6-75b6-4fea-a636-5ef7c319c46e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>928b0e13-3ced-467c-95b9-1c5f10d88cc3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
							Select
						</value>
      <webElementGuid>713267ee-0997-496e-ac77-fe1cfff5947f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_phF_form_t0_edCustomerID_pnl_tlb_ul&quot;)/li[1]/div[@class=&quot;toolsBtn connotation-success&quot;]/div[@class=&quot;toolBtnNormal&quot;]</value>
      <webElementGuid>aa4eddd1-23d5-43ca-a379-442157afa322</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/iframe_sales_order_detail</value>
      <webElementGuid>4f32860e-1aa4-4615-8f2a-b1795193297b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//ul[@id='ctl00_phF_form_t0_edCustomerID_pnl_tlb_ul']/li/div/div</value>
      <webElementGuid>1e1db059-8d76-411a-86f6-f1710ab6d24f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Select - Customer'])[1]/following::div[4]</value>
      <webElementGuid>cdd35e90-796b-43f8-8e7f-d1cf33c414fa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Export'])[11]/following::div[11]</value>
      <webElementGuid>2f797c81-36a9-4693-b1ce-6f7cc2ff794d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='All Records'])[1]/preceding::div[25]</value>
      <webElementGuid>78a45c54-ec06-48a1-a7b3-1b0f7f07e479</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Customer ID'])[1]/preceding::div[40]</value>
      <webElementGuid>8b230414-0e80-48fc-abda-74a606d87bad</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//table[4]/tbody/tr[2]/td/div/div/ul/li/div/div</value>
      <webElementGuid>663feb00-656c-409f-a932-de7ddb0c7ea6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
							Select
						' or . = '
							Select
						')]</value>
      <webElementGuid>ac5df714-bb9e-49ff-bad4-30565627c1ef</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
