<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_select_popup_branch</name>
   <tag></tag>
   <elementGuidId>727ca3ad-f5c0-4f94-aa81-c6cd365d8470</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//ul[@id='ctl00_phG_tab_t0_grid_lv0_edBranchID_pnl_tlb_ul']/li/div/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ctl00_phG_tab_t0_grid_lv0_edBranchID_pnl_tlb_ul > li > div.toolsBtn.connotation-success > div.toolBtnNormal</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>2934d332-afd0-47d4-8478-1c5b332c43d2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>toolBtnNormal</value>
      <webElementGuid>a59c53da-851d-42fe-ab9a-79ca2cec04be</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>6a5827d0-5b32-47b0-a2d2-f1e76469cf36</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
							Select
						</value>
      <webElementGuid>fbd00399-f585-4e05-bb8f-b2f27fc44e8e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_phG_tab_t0_grid_lv0_edBranchID_pnl_tlb_ul&quot;)/li[1]/div[@class=&quot;toolsBtn connotation-success&quot;]/div[@class=&quot;toolBtnNormal&quot;]</value>
      <webElementGuid>c642f632-573b-4cac-ae6f-e726645f5483</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/iframe_sales_order_detail</value>
      <webElementGuid>6d65417a-9ac0-4f4f-b17b-23d38d0cfacd</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//ul[@id='ctl00_phG_tab_t0_grid_lv0_edBranchID_pnl_tlb_ul']/li/div/div</value>
      <webElementGuid>ae3e52c0-c871-4caf-8a3a-4276fffbc993</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Reset layout'])[13]/following::div[10]</value>
      <webElementGuid>86c5daa1-3860-4763-8a4c-114c8a84ec62</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Save layout'])[13]/following::div[13]</value>
      <webElementGuid>255ac085-7e6b-4f11-8864-62bf87b3c3e4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Branch ID'])[1]/preceding::div[18]</value>
      <webElementGuid>76144b43-2c66-4993-bf10-a24692dde2cb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Branch Name'])[1]/preceding::div[22]</value>
      <webElementGuid>e7e6fcb6-aa37-4287-8097-b1676ace1074</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//table[5]/tbody/tr[2]/td/div/div/ul/li/div/div</value>
      <webElementGuid>7ae862a6-ce30-4805-9028-b74e49a80589</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
							Select
						' or . = '
							Select
						')]</value>
      <webElementGuid>98b4d524-a924-49cd-88b4-7d0452e4aea6</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
