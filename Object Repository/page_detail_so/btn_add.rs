<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_add</name>
   <tag></tag>
   <elementGuidId>e7bdedac-72b7-4143-bf56-80c97e95527e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;_ctl00_phG_tab_t0_grid_lv0_edUOM&quot;)/div[@class=&quot;controlCont&quot;]/div[@class=&quot;buttonsCont&quot;]/div[@class=&quot;sprite-icon control-icon&quot;][count(. | //*[@type = 'sel']) = count(//*[@type = 'sel'])]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#_ctl00_phG_tab_t0_grid_lv0_edUOM > div.controlCont > div.buttonsCont > div.sprite-icon.control-icon</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//div[@type='sel'])[18]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>80136e56-4c9e-45db-b2a7-1eedb072dd70</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>sprite-icon control-icon</value>
      <webElementGuid>df428915-d10a-4f34-965f-cb2a983160c6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>au-target-id</name>
      <type>Main</type>
      <value>69</value>
      <webElementGuid>790882b1-aacd-46e2-8ff2-49eb6c60cf8f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;_ctl00_phG_tab_t0_grid_lv0_edUOM&quot;)/div[@class=&quot;controlCont&quot;]/div[@class=&quot;buttonsCont&quot;]/div[@class=&quot;sprite-icon control-icon&quot;]</value>
      <webElementGuid>863a15a0-2219-4023-b3e2-0aca0074341e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>3ad76eec-3f36-4e21-980e-936f77c7aeb1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/iframe_so_details</value>
      <webElementGuid>cfe1ae21-d14e-4926-a567-c6a9eba2a2e3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>icon</name>
      <type>Main</type>
      <value>SelectorH</value>
      <webElementGuid>7e9268ed-5e13-46ef-ae99-ec86a8e5ac6a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>sel</value>
      <webElementGuid>cff4d3f6-dd8f-4267-9478-da9d92e57f9f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//div[@type='sel'])[18]</value>
      <webElementGuid>c0e2bdf5-16c3-4c1a-9054-aea3d3bd15a0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='_ctl00_phG_tab_t0_grid_lv0_edUOM']/div/div[2]/div</value>
      <webElementGuid>b6d552aa-27aa-48a2-ac45-5fe0aa94466b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Default'])[3]/following::div[18]</value>
      <webElementGuid>5f8c7912-c009-4a80-80f3-fa90b48b1727</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SP0003'])[2]/following::div[18]</value>
      <webElementGuid>5a1c4a8f-2076-45a5-9fed-a62efbac2b88</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Inventory ID:'])[1]/preceding::div[26]</value>
      <webElementGuid>8f020a3a-80ad-4060-b51e-cb8ae4adcd45</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Require Appointment'])[4]/preceding::div[44]</value>
      <webElementGuid>49df7066-d151-4e65-94ba-4c0511109ac0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//qp-editor-wrapper[2]/div/div/div[2]/div</value>
      <webElementGuid>b2519af1-cc4b-4a1b-a121-8762a6e790a6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@type = 'sel']</value>
      <webElementGuid>dab89269-9eae-41d0-9db0-66d8b2b0cc30</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='_ctl00_phG_tab_t0_grid_lv0_edSiteID']/div/div[2]/div/div</value>
      <webElementGuid>2a4da445-6da2-4119-b0bd-836689848028</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Default'])[3]/following::div[38]</value>
      <webElementGuid>01a6a67b-d550-4e93-ab02-415032b89540</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SP0003'])[2]/following::div[38]</value>
      <webElementGuid>106362a3-238c-4fcb-8f12-5b0653c41004</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Inventory ID:'])[1]/preceding::div[25]</value>
      <webElementGuid>f82e724a-54de-401b-b66b-12b12a04ac44</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Require Appointment'])[4]/preceding::div[43]</value>
      <webElementGuid>08e68d77-964f-4260-90e4-69cd5b7e4d77</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//qp-editor-wrapper[2]/div/div/div[2]/div/div</value>
      <webElementGuid>ba899379-901e-4d86-865f-1be302bb570a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='_ctl00_phG_tab_t0_grid_lv0_edInventoryID']/div/div[2]/div/div</value>
      <webElementGuid>9d83fdd7-a5bf-4aaf-b93e-a75e89247f1a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Inventory ID:'])[1]/preceding::div[29]</value>
      <webElementGuid>f3b2c921-32d2-46ea-bbd5-4e4729f399bc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Require Appointment'])[4]/preceding::div[47]</value>
      <webElementGuid>9be3fd15-7080-4537-bf0b-42a5cd026553</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='ctl00_phDS_ds_ToolBar_insert']/div/qp-hyper-icon/div/div/div</value>
      <webElementGuid>d169db62-c6c8-41ae-9f37-fdac5054d327</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[3]/div/qp-hyper-icon/div/div/div</value>
      <webElementGuid>101b7a0f-2f2c-496c-98e4-6328268901e6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='_ctl00_phG_tab_t0_grid_lv0_edBranchID']/div/div[2]/div/div</value>
      <webElementGuid>f0daf2ba-10f2-404a-8be4-d331d43f9496</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Default'])[3]/following::div[19]</value>
      <webElementGuid>96378ceb-1c08-47bf-ac35-b84b593c760b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SP0003'])[2]/following::div[19]</value>
      <webElementGuid>214ee6f3-0da3-44d0-a435-bf0ba53b0632</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Inventory ID:'])[1]/preceding::div[44]</value>
      <webElementGuid>34db9e3e-a998-486c-9b6d-9a0f9deb12de</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Require Appointment'])[4]/preceding::div[62]</value>
      <webElementGuid>b360b40a-5db8-47f4-bede-603a3b170225</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td/div/qp-editor-wrapper/div/div/div[2]/div/div</value>
      <webElementGuid>e335e49a-4098-4bcb-b83b-aa66a88a79e5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//ul[@id='ctl00_phG_tab_t0_grid_at_tlb_ul']/li[2]/div/div</value>
      <webElementGuid>abb6ec3f-611e-4dd0-948c-5e7b5eb8c041</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='ROT/RUT Details'])[1]/following::div[12]</value>
      <webElementGuid>033dd567-952f-4f37-a506-991ac1ba6e94</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Totals'])[1]/following::div[12]</value>
      <webElementGuid>59216a9c-e9b5-40d5-a8a3-516d269bef49</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Add Items'])[1]/preceding::div[12]</value>
      <webElementGuid>c20921c1-46f1-4fef-8649-bbf965e44849</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Add Matrix Items'])[1]/preceding::div[14]</value>
      <webElementGuid>c85cd392-e9a5-4d94-8d22-d570bccf5305</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td/div/table/tbody/tr/td/div/div/ul/li[2]/div/div</value>
      <webElementGuid>17063402-1759-411d-821b-cec10f5fd781</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='ctl00_phF_form_t0_edCustomerLocationID']/div/div[2]/div/div</value>
      <webElementGuid>a49f0699-ac69-4e86-ac66-48167bd99ee9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Location:'])[1]/following::div[8]</value>
      <webElementGuid>dce5fc55-e4ec-4dd0-bdfb-e56c7980f24e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Customer:'])[1]/following::div[25]</value>
      <webElementGuid>820d801b-2a56-410c-bb22-aae89811ae08</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Contact:'])[1]/preceding::div[6]</value>
      <webElementGuid>84c53627-13f8-43b9-a9a1-2c3b293b9278</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Currency:'])[1]/preceding::div[23]</value>
      <webElementGuid>14f2280f-702d-42d7-a2bf-3b780dee5c19</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div/div/div/div/div/div[2]/div/div[2]/div[2]/qp-editor-wrapper/div/div/div[2]/div/div</value>
      <webElementGuid>5448232b-2094-4989-a615-5e6679a3b81a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='ctl00_phF_form_t0_edCustomerID']/div/div[2]/div/div</value>
      <webElementGuid>7359ed20-e138-491e-af41-518023b8686d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Customer:'])[1]/following::div[8]</value>
      <webElementGuid>8622212b-236b-4eeb-842a-b73ebe8fcd31</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='${translation.language}'])[9]/following::div[23]</value>
      <webElementGuid>53f98d07-5e2e-4a10-a493-623c0394cd0c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Location:'])[1]/preceding::div[6]</value>
      <webElementGuid>7e35ebc3-1d87-4951-a6ee-8ddbdb04d461</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Contact:'])[1]/preceding::div[23]</value>
      <webElementGuid>d5591869-d877-46b4-950f-89b99e8a0552</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div/div/div/div/div/div/div/div/div[2]/div[2]/qp-editor-wrapper/div/div/div[2]/div/div</value>
      <webElementGuid>59075bbc-9a9f-4764-bd20-3f121633d45c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='ctl00_phF_form_t0_edOrderType']/div/div[2]/div/div</value>
      <webElementGuid>ea66861b-79a7-4835-85d9-766f17a3a4e2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Order Type:'])[1]/following::div[8]</value>
      <webElementGuid>ba298323-d7fc-452b-b534-37e80d8d7ba6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='User-Defined Fields'])[1]/following::div[25]</value>
      <webElementGuid>de66ec8c-209a-40ca-9952-c67cc21cb1b1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Order Nbr.:'])[1]/preceding::div[2]</value>
      <webElementGuid>290906a3-f0eb-41fd-ade3-8f9b1e943e0a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Status:'])[1]/preceding::div[15]</value>
      <webElementGuid>1fae3fb0-f58b-4caf-820c-1055e469cf63</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div/div/div/div/div/div[2]/div[2]/qp-editor-wrapper/div/div/div[2]/div/div</value>
      <webElementGuid>f98c53ef-a797-48b2-9cbd-0c06701056ae</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='ctl00_phDS_ds_ToolBar_Delete']/div/qp-hyper-icon/div/div/div</value>
      <webElementGuid>d8e44feb-f150-4fd2-9db5-703078814844</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//ul[2]/li[6]/div/qp-hyper-icon/div/div/div</value>
      <webElementGuid>9fbefdcc-07cb-4724-9c3c-fd472a059cd2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='ctl00_phDS_ds_ToolBar_Insert']/div</value>
      <webElementGuid>29fb8608-c7f8-4ac4-8c93-6cc300b39bcd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Hold'])[1]/following::div[17]</value>
      <webElementGuid>4201ec41-a92c-42c1-b256-86e0722f20ed</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='State diagram'])[1]/following::div[70]</value>
      <webElementGuid>9515560b-df7e-4769-8d8f-e447da8fc724</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Hold'])[2]/preceding::div[30]</value>
      <webElementGuid>68b92e4a-78e0-4755-9fcf-d74dde65693f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Nothing in progress'])[1]/preceding::div[33]</value>
      <webElementGuid>b74d0a90-23a8-4834-978e-e8086c2a443c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//ul[2]/li[5]/div</value>
      <webElementGuid>378dc60c-51e1-4c63-a27d-44958e2774df</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
