<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>form_orderNbr</name>
   <tag></tag>
   <elementGuidId>87987e92-201a-4c0d-9e55-c5547faa3a99</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;ctl00_phF_form_t0_edOrderNbr_text&quot;)[count(. | //*[@name = 'ctl00$phF$form$t0$edOrderNbr$text' and @type = 'text' and @id = 'ctl00_phF_form_t0_edOrderNbr_text' and @class = 'editor' and @ref_element = 'Object Repository/iframe_sales_order_detail']) = count(//*[@name = 'ctl00$phF$form$t0$edOrderNbr$text' and @type = 'text' and @id = 'ctl00_phF_form_t0_edOrderNbr_text' and @class = 'editor' and @ref_element = 'Object Repository/iframe_sales_order_detail'])]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ctl00_phF_form_t0_edOrderNbr_text</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>id(&quot;ctl00_phF_form_t0_edOrderNbr_text&quot;)[count(. | //*[@name = 'ctl00$phF$form$t0$edOrderNbr$text' and @type = 'text' and @id = 'ctl00_phF_form_t0_edOrderNbr_text' and @class = 'editor']) = count(//*[@name = 'ctl00$phF$form$t0$edOrderNbr$text' and @type = 'text' and @id = 'ctl00_phF_form_t0_edOrderNbr_text' and @class = 'editor'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>0e024670-3422-406a-9254-17ac1daee298</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ctl00$phF$form$t0$edOrderNbr$text</value>
      <webElementGuid>bfcbc834-28ee-4ca5-9375-a1dc3621e12d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>110a1c15-7ddb-4df1-bff3-0078a19552c7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ctl00_phF_form_t0_edOrderNbr_text</value>
      <webElementGuid>6e3b8175-8ef7-4a67-99c4-c3c4a537cde9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>18201</value>
      <webElementGuid>a537a52b-1a82-4b17-8add-c0c840b6f5bf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>editor</value>
      <webElementGuid>924aa84e-5880-42cc-9bcf-8bedc33153fd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>a388c7e3-ba82-4bf0-83cd-50193cf754fe</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocorrect</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>ea5e075b-9e24-4acc-a936-43a447e653e6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_phF_form_t0_edOrderNbr_text&quot;)</value>
      <webElementGuid>70bf019f-346b-47fe-93a1-8112b940fafb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/iframe_sales_order_detail</value>
      <webElementGuid>5f21d419-324d-4ff3-b254-c714c3a2ec94</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='ctl00_phF_form_t0_edOrderNbr_text']</value>
      <webElementGuid>53ecabb6-a977-4464-a4f5-9cb55d85b64b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='ctl00_phF_form_t0_edOrderNbr']/div/div/div/input</value>
      <webElementGuid>8d374003-fce7-40d2-b1bb-0b15c913e55c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div[2]/div[2]/qp-editor-wrapper/div/div/div/div/input</value>
      <webElementGuid>f1a3e942-b105-47e4-9225-7b4fd42b987b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@name = 'ctl00$phF$form$t0$edOrderNbr$text' and @type = 'text' and @id = 'ctl00_phF_form_t0_edOrderNbr_text']</value>
      <webElementGuid>431fdc7f-f5c9-4397-8576-8e13c9cb19b5</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
