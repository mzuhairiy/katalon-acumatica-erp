<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>field_orderNbr</name>
   <tag></tag>
   <elementGuidId>f9c4c126-58a9-4207-99f0-316d20a04bc5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#ctl00_phF_form_t0_edOrderNbr > div.controlCont</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='ctl00_phF_form_t0_edOrderNbr']/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>68004839-66fe-4e61-b58d-222af7958ee0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>controlCont</value>
      <webElementGuid>2114b83d-0070-4c5d-888d-3da2055aedff</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_phF_form_t0_edOrderNbr&quot;)/div[@class=&quot;controlCont&quot;]</value>
      <webElementGuid>65ff0812-85f4-40b7-a13b-59baca132751</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/iframe_sales_order_detail</value>
      <webElementGuid>105b5033-a6c2-4a92-8a7e-cbaa1d9dc6fe</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='ctl00_phF_form_t0_edOrderNbr']/div</value>
      <webElementGuid>59c341e3-1d54-4b48-ad97-fd0618b1c256</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Order Nbr.:'])[1]/following::div[3]</value>
      <webElementGuid>944f4ad4-b386-46d9-b153-7bb869d90d4c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Order Type:'])[1]/following::div[16]</value>
      <webElementGuid>718c7d86-a0f0-41b2-b783-9828aaf7aa45</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Status:'])[1]/preceding::div[7]</value>
      <webElementGuid>b960ad20-9097-4cda-83f6-2bef7b18cd79</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Open'])[1]/preceding::div[8]</value>
      <webElementGuid>80260ea1-1e67-4455-bf45-49b84e4efc66</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div[2]/div[2]/qp-editor-wrapper/div/div</value>
      <webElementGuid>b3e0e9b9-b783-4a2e-8833-b3cbd5e0143c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
