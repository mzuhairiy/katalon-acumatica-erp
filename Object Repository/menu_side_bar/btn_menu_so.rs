<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_menu_so</name>
   <tag></tag>
   <elementGuidId>d12e59ab-e2af-438b-b429-3edefb614d48</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.list-group-item.selected.active</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='listModules']/div[8]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>84ebff4d-0ede-4b8f-a11c-d8da211311f0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>list-group-item selected active</value>
      <webElementGuid>541fe5dd-5588-4d97-af93-6c3b9741e837</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-id</name>
      <type>Main</type>
      <value>e2c3849a-6280-41df-81f3-552b91adfae5</value>
      <webElementGuid>40bd63d3-d631-4d31-b51a-d5ead3b8221e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Sales Orders</value>
      <webElementGuid>16c9c974-005c-497d-8f95-eb06c1596513</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;listModules&quot;)/div[@class=&quot;list-group-item selected active&quot;]</value>
      <webElementGuid>7329796b-7c23-447e-b538-67ff7153e331</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='listModules']/div[8]</value>
      <webElementGuid>1a76763f-c312-4365-a8f3-fee37efd8720</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Customization'])[1]/following::div[1]</value>
      <webElementGuid>f766dd74-b15b-4010-a764-1012223ae8d1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Receivables'])[1]/following::div[2]</value>
      <webElementGuid>c52fd49b-cb50-45d7-b449-f75a1df8cd69</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Purchases'])[1]/preceding::div[1]</value>
      <webElementGuid>d56f6dbf-841d-49d7-969e-0da63fe7e27a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div[8]</value>
      <webElementGuid>f8d6201c-711e-48fd-ad5e-1f40ab628ab3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Sales Orders' or . = 'Sales Orders')]</value>
      <webElementGuid>cdbf9ed9-4048-45dc-b2f5-2966de713793</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
