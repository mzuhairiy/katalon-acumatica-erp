import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.webui.keyword.builtin.WaitForElementNotVisibleKeyword as WaitForElementNotVisibleKeyword
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Login'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('menu_side_bar/btn_menu_so'))

//WebUI.waitForElementClickable(findTestObject('page_sub_menu/btn_transaction_so'), 30)
WebUI.click(findTestObject('page_sub_menu/btn_transaction_so'))

//WebUI.switchToFrame(findTestObject('page_detail_so/iframe_so_details'), 30)
WebUI.waitForPageLoad(30)

WebUI.waitForElementClickable(findTestObject('page_so_listing/btn_add_so_listing'), 0)

WebUI.click(findTestObject('page_so_listing/btn_add_so_listing'))

WebUI.click(findTestObject('page_detail_so/btn_select_order_type'))

WebUI.click(findTestObject('page_detail_so/ot_sales_order'))

WebUI.click(findTestObject('page_detail_so/btn_select_popup_ordertype'))

WebUI.click(findTestObject('page_detail_so/btn_select_customer'))

WebUI.click(findTestObject('page_detail_so/cust_alta_ace'))

WebUI.click(findTestObject('page_detail_so/btn_select_popup_cust'))

WebUI.click(findTestObject('page_detail_so/btn_add_details_item'))

WebUI.click(findTestObject('page_detail_so/btn_select_branch'))

WebUI.click(findTestObject('page_detail_so/branch_prodwhole'))

WebUI.click(findTestObject('page_detail_so/btn_select_popup_branch'))

WebUI.click(findTestObject('page_detail_so/btn_select_inventory'))

WebUI.click(findTestObject('page_detail_so/inv_acer_laptop'))

WebUI.click(findTestObject('page_detail_so/btn_select_popup_inventory'))

WebUI.click(findTestObject('page_detail_so/field_qty'), FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('page_detail_so/form_qty'), '1.00')

WebUI.sendKeys(findTestObject('page_detail_so/form_qty'), 'Enter')

WebUI.click(findTestObject('page_detail_so/btn_save'))

WebUI.click(findTestObject('page_detail_so/field_orderNbr'))

WebUI.closeBrowser()

